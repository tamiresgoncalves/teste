﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace QAchallenge
{
    [TestClass]
    public class UnitTest1
    {
        private const string Url = "http://localhost:3000/";
        private IWebDriver driver; 

        [TestMethod]
        public void TestMethod1()
        {
            driver = new FirefoxDriver();
            driver.Navigate().GoToUrl(Url);
            driver.Manage().Window.Maximize();

            IList<IWebElement> list = driver.FindElements(By.CssSelector("//div[@id='app']"));
            List<int> lista = new List<int>(Convert.ToInt32(list));

            var sorted = new List<int>();
            sorted.AddRange(lista.OrderBy(o => o));
            Assert.IsTrue(lista.SequenceEqual(sorted));

            driver.Close();
            driver.Quit();
        }
    }
}
